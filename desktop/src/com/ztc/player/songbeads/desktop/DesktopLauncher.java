package com.ztc.player.songbeads.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ztc.player.songbeads.SongBeadsPlayer;

/**
 * @author Omer Gillani
 *
 */
public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.height = 800;
		config.width = 720;
		
		new LwjglApplication(new SongBeadsPlayer(), config);
	}
}
