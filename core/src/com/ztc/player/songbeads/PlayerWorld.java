package com.ztc.player.songbeads;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ztc.player.songbeads.model.BallModel;
import com.ztc.player.songbeads.model.ThreadModel;
import com.ztc.player.songbeads.model.WindChimeModel;
import com.ztc.player.songbeads.util.PlayerUtility;

/**
 * This class is responsible to update player items states
 * 
 * @author Omer Gillani
 * 
 */

public class PlayerWorld {
	
	public static final String SONGS_DIR = "/songs";

	/** Player Wind Chime **/
	private WindChimeModel windChimeModel;
	
	private ThreadModel newPlayListThread;

	/**
	 * 
	 */
	public PlayerWorld() {

		// Step 1
		initializeChime();

		// Step 2
		initializeThreads();

		// Step 3
		initializeBalls();
	}

	/**
	 * 
	 */
	private void initializeBalls() {

		float ballsYPosStart = Gdx.graphics.getHeight()
				- PlayerAssetManager.getChimeHeadTexture().getHeight()
				- PlayerAssetManager.getThreadTexture().getHeight() + 20;

		float xPos = PlayerUtility.calculateThreadXStartPos()
				+ PlayerAssetManager.getThreadTexture().getWidth() / 2
				- (PlayerAssetManager.getBall().getWidth() / 2);

		float yPos = ballsYPosStart;

		FileHandle [] files = Gdx.files.external(SONGS_DIR).list();
		
		int songCounter = 0;

		for (int i = 0; i < windChimeModel.getThreadModels().size - 1; i++) {

			yPos = ballsYPosStart + 100;

			for (int j = 0; j < 6; j++) {

				Sprite ballSprite = new Sprite(new Sprite(
						PlayerAssetManager.getBall()));
				ballSprite.setPosition(xPos, yPos);

				BallModel ballModel = new BallModel(ballSprite);

				if (songCounter < files.length) {
					ballModel.setSongPath(SONGS_DIR + "/"
							+ files[songCounter++].name());
				}
				
				windChimeModel.getThreadModels().get(i).getBalls()
						.add(ballModel);

				yPos += 200;
			}

			xPos += 235;
		}
		
//		xPos += 235;
		xPos += 135;
		yPos = ballsYPosStart + 100;
		//Add balls to the new play-list thread
		for(int i = 0; i < files.length; i++) {
			
			Sprite ballSprite = new Sprite(new Sprite(
					PlayerAssetManager.getBall()));
			
			ballSprite.setPosition(xPos, yPos);
			
			BallModel ballModel = new BallModel(ballSprite);
			
			ballModel.setSongPath(SONGS_DIR + "/"
					+ files[i].name());
			
			newPlayListThread.getBalls().add(ballModel);
			
			yPos += 200;
		}
	}

	/**
	 * Initialize chime threads
	 */
	private void initializeThreads() {
		float threadStartXPos = (Gdx.graphics.getWidth() / 2)
				- (PlayerAssetManager.getChimeHeadTexture().getWidth() / 2)
				+ 20;

		float threadStartYpos = PlayerUtility.calculateChimeHeadYPos()
				- PlayerAssetManager.getThreadTexture().getHeight();
		
		float xPos = threadStartXPos; // 220;
		float yPos = threadStartYpos; // 50

		for (int i = 0; i < 4; i++) {

			Sprite threadSprite = new Sprite(new Sprite(
					PlayerAssetManager.getThreadTexture()));

			threadSprite.setPosition(xPos, i == 0 || i == 3 ? yPos + 27 : yPos);

			ThreadModel threadModel = new ThreadModel(threadSprite);

			windChimeModel.getThreadModels().add(threadModel);

			xPos += 235;
		}
		
		xPos -= 100;
		
		// Create new play-list sprite
		Sprite playListSprite = new Sprite(new Sprite(
				PlayerAssetManager.getThreadTexture()));
		
		playListSprite.setPosition(xPos, yPos);
		
		newPlayListThread = new ThreadModel(playListSprite);
	}

	/**
	 * 
	 */
	private void initializeChime() {
		Sprite chimeHeadSprite = new Sprite(
				PlayerAssetManager.getChimeHeadTexture());
//		chimeHeadSprite.scale(-0.5f);
		windChimeModel = new WindChimeModel(chimeHeadSprite);

		float chimeHeadXPos = PlayerUtility.calculateChimeHeadXpos(); // 200
		float chimeHeadYPos = PlayerUtility.calculateChimeHeadYPos(); // 373

//		tempVector.x = chimeHeadSprite.getBoundingRectangle().width;
//		tempVector.y = chimeHeadSprite.getBoundingRectangle().height;
//		tempVector.z = 0;
		
//		PlayerRenderer2D.cam.project(tempVector);
		
		// handle scaling
//		chimeHeadYPos = Gdx.graphics.getHeight() - tempVector.y;

		chimeHeadSprite.setPosition(chimeHeadXPos, chimeHeadYPos);
	}

	/**
	 * 
	 * @param delta
	 */
	public void update(float delta) {

	}

	/**
	 * 
	 * @return
	 */
	public WindChimeModel getWindChimeModel() {
		return windChimeModel;
	}

	/**
	 * 
	 * @param windChimeModel
	 */
	public void setWindChimeModel(WindChimeModel windChimeModel) {
		this.windChimeModel = windChimeModel;
	}
	
	/**
	 * 
	 * @return
	 */
	public ThreadModel getNewPlayListThread() {
		return newPlayListThread;
	}
	
	/**
	 * 
	 * @param newPlayListThread
	 */
	public void setNewPlayListThread(ThreadModel newPlayListThread) {
		this.newPlayListThread = newPlayListThread;
	}
	
	/**
	 * 
	 */
	public void dispose() {
		
	}
}
