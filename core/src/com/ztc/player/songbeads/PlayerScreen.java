package com.ztc.player.songbeads;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.ztc.player.songbeads.renderer.PlayerRenderer2D;

/**
 * @author Gillani
 *
 */
public class PlayerScreen implements Screen {
	
	/** Player Renderer **/
	private PlayerRenderer2D renderer;
	
	/** Player World **/
	private PlayerWorld world;

	/**
	 * 
	 */
	public PlayerScreen() {
		Gdx.app.log("GameScreen", "Attached");
		
        @SuppressWarnings("unused")
		float screenWidth = Gdx.graphics.getWidth();
        @SuppressWarnings("unused")
		float screenHeight = Gdx.graphics.getHeight();
          
		world = new PlayerWorld();
		renderer = new PlayerRenderer2D(world);
//		gestureListener = new PlayerGestureListener(world);
		
		InputMultiplexer multiplexer = new InputMultiplexer();
		
		multiplexer.addProcessor((new GestureDetector(renderer)));
		multiplexer.addProcessor(renderer);
		
		Gdx.input.setInputProcessor(multiplexer);
	}
	
	@Override
	public void render(float delta) {
		world.update(delta);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		renderer.resize(width, height);
	}

	@Override
	public void show() {
		Gdx.app.log("PlayerScreen", "Show");
	}

	@Override
	public void hide() {
		Gdx.app.log("PlayerScreen", "Hide");
	}

	@Override
	public void pause() {
		Gdx.app.log("PlayerScreen", "Pause");
	}

	@Override
	public void resume() {
		Gdx.app.log("PlayerScreen", "Resume");
	}

	@Override
	public void dispose() {
		world.dispose();
		renderer.dispose();
	}
}
