package com.ztc.player.songbeads.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.io.Mpg123Decoder;
import com.badlogic.gdx.backends.android.AndroidMusic;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.ztc.player.songbeads.PlayerAssetManager;
import com.ztc.player.songbeads.PlayerWorld;
import com.ztc.player.songbeads.model.BallModel;
import com.ztc.player.songbeads.model.ThreadModel;
import com.ztc.player.songbeads.util.PlayerUtility;

/**
 * This class is responsible for rendering
 * 
 * 
 * @author Omer Gillani
 * 
 */

public class PlayerRenderer2D extends PlayerRenderer implements GestureListener, InputProcessor {

	public static final int CHIME_VIEW = 0;
	public static final int THREAD_VIEW = 1;
	public static final int BALL_VIEW = 2;
	public static final int NEW_PLAYLIST_VIEW = 3;

	public static final float CHIME_VIEW_ZOOM = 2.0f;
	public static final float THREAD_VIEW_ZOOM = 1.0f;
	public static final float BALL_VIEW_ZOOM = 0.3f;

	/** Player World **/
	private PlayerWorld world;

	/** Show 3D space in 2D **/
	public OrthographicCamera cam;
	public OrthographicCamera playListCam;
	public OrthographicCamera staticCam;

	public Vector3 tempVector = new Vector3();

	/** Camera testing **/
	static final int WORLD_WIDTH = 100;
	static final int WORLD_HEIGHT = 100;

	/** Current view state of player **/
	private int viewState = CHIME_VIEW;

	private Vector3 afterUnProject = new Vector3();

	/** Rectangle to detect collision **/ 
	private Rectangle rectangle = new Rectangle();

	/** Selected Thread **/
	private ThreadModel selectedThread = null;

	/** Selected Ball **/
	private BallModel selectedBall = null;

	/** Current playing music **/
	private Music currentMusic = null;
	
	/** Decoder to get total length of song **/
	private Mpg123Decoder currentDecoder = null;

	/** Playing Effects **/
	private ParticleEffect particleEffect;

	/** Shape Renderer **/
	private ShapeRenderer shapeRenderer;
	
	/** Vertex Shader **/
//    private String vertexShader;
    
    /** Fragment Shader **/
//    private String fragmentShader;
    
    /** Shader Program **/
//    private ShaderProgram shaderProgram;

	/**
	 * 
	 */
	public PlayerRenderer2D(PlayerWorld world) {

		this.world = world;

		shapeRenderer = new ShapeRenderer();

		createCamera();
		createParticleEffect();
//		createShaders();
	}

	/**
	 * Initialize shader
	 */
/*	private void createShaders() {
		
        vertexShader = Gdx.files.internal("data/vertex.glsl").readString();
        fragmentShader = Gdx.files.internal("data/fragment.glsl").readString();
        shaderProgram = new ShaderProgram(vertexShader,fragmentShader);
		
	}*/

	/**
	 * 
	 */
	private void createParticleEffect() {
		particleEffect = new ParticleEffect();
		particleEffect.load(Gdx.files.internal("data/playeffects.p"),
				Gdx.files.internal("data"));
	}

	/**
	 * 
	 * @return
	 */
	private OrthographicCamera createCamera() {

		// World Camera
		cam = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		cam.zoom = CHIME_VIEW_ZOOM;

		cam.position.set(cam.viewportWidth / 2f, (cam.viewportHeight / 2f), 0);
		
		cam.translate(0, -150, 0);

		cam.update();
		
		// Camera for new play list thread
		playListCam = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		playListCam.zoom = CHIME_VIEW_ZOOM;

		playListCam.position.set(cam.viewportWidth / 2f, (cam.viewportHeight / 2f), 0);
		
		playListCam.translate(0, -150, 0);
		
		playListCam.update();
		
		// Static Camera for Background
		staticCam = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		staticCam.setToOrtho(true);
		staticCam.update();

		return cam;
	}

	/**
	 * Render
	 */
	public void render() {

		// We draw a black background. This prevents flickering.
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		cam.update();
		staticCam.update();
		playListCam.update();

		SpriteBatch batch = PlayerAssetManager.getSpriteBatch();
		
		batch.setProjectionMatrix(staticCam.combined);
		batch.begin();
		batch.draw(PlayerAssetManager.getBackground(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
		
		if(viewState == NEW_PLAYLIST_VIEW) {
			batch.setProjectionMatrix(playListCam.combined);
			batch.begin();	
			world.getNewPlayListThread().getSprite().draw(batch);
			
			for (BallModel ballModel : world.getNewPlayListThread().getBalls()) {
				ballModel.getSprite().draw(batch);
			}
			
			batch.end();
		}
		
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
//		batch.setShader(shaderProgram);
		world.getWindChimeModel().getSprite().draw(batch);

		// Draw Threads
		for (ThreadModel threadModel : world.getWindChimeModel()
				.getThreadModels()) {

			if (threadModel.isVisible()) {

				threadModel.getSprite().draw(batch);

				for (BallModel ballModel : threadModel.getBalls()) {
					ballModel.getSprite().draw(batch);
				}
			}
		}
		

		if (currentMusic != null && selectedBall != null/*currentMusic.isPlaying()*/) {
			
			if(currentMusic.isPlaying()) {
				
				float position = currentMusic.getPosition();
				float length = currentDecoder.getLength();
				
				particleEffect.update(Gdx.graphics.getDeltaTime());
				particleEffect.draw(batch);
				
				if (particleEffect.isComplete()) {
					
					particleEffect.start();
					particleEffect.getEmitters().get(0).setMaxParticleCount((int) (length - position));
					particleEffect.getEmitters().get(0).setMinParticleCount(0);
					
				}
					
			}
		}
		
		batch.end();
		
		drawTimeLine();
	}

	/**
	 * 
	 */
	private void drawTimeLine() {
		
		if (currentMusic != null && selectedBall != null) {
			
			calculateRadius();
			
			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			shapeRenderer.setProjectionMatrix(cam.combined);
			shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(new Color(0, 1, 0, 0.3f));
			shapeRenderer.circle(selectedBall.getSprite()
					.getBoundingRectangle().x
					+ selectedBall.getSprite().getBoundingRectangle()
							.getWidth() / 2, selectedBall.getSprite()
					.getBoundingRectangle().y
					+ selectedBall.getSprite().getBoundingRectangle()
							.getHeight() / 2, calculateRadius());
			shapeRenderer.end();
			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
	}

	private float calculateRadius() {
		
		float totalRadius = selectedBall.getSprite().getBoundingRectangle().getWidth() / 2;
		float position = currentMusic.getPosition();
		float length = currentDecoder.getLength();
		
		float factor = length / totalRadius;
		
		float radius = position / factor;
		
		Gdx.app.log("Song lenght", length + "");
		Gdx.app.log("Total Radius", totalRadius + "");
		Gdx.app.log("Radius", radius + "");
		Gdx.app.log("Position", position + "");
		
		return radius;
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("PlayerRederer2D", "Resize screen");
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		afterUnProject.x = x;
		afterUnProject.y = y;

		cam.unproject(afterUnProject);

		boolean itemTap = false;

		/*if (count == 2) */{

			itemTap = false;

			Array<ThreadModel> threadModels = world.getWindChimeModel()
					.getThreadModels();

			if (viewState == CHIME_VIEW) {
				
				int i;
				for (i = 0; i < world.getWindChimeModel().getThreadModels().size; i++) {
					
					if (world.getWindChimeModel().getThreadModels().get(i)
							.getBalls().size > 0) {
						rectangle.x = world.getWindChimeModel()
								.getThreadModels().get(i).getBalls().get(0)
								.getSprite().getBoundingRectangle().x;

						rectangle.y = world.getWindChimeModel()
								.getThreadModels().get(i).getSprite()
								.getBoundingRectangle().y;
					} else {
						rectangle.x = world.getWindChimeModel()
								.getThreadModels().get(i).getSprite()
								.getBoundingRectangle().x;

						rectangle.y = world.getWindChimeModel()
								.getThreadModels().get(i).getSprite()
								.getBoundingRectangle().y;
					}

					rectangle.height = world.getWindChimeModel()
							.getThreadModels().get(i).getSprite()
							.getBoundingRectangle().height;

					rectangle.width = world.getWindChimeModel()
							.getThreadModels().get(0).getBalls().get(0)
							.getSprite().getBoundingRectangle().getWidth();

					if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {

						// Gdx.app.log("Thread Touch Detection", "Thread " + i);

						itemTap = true;

						if (world.getWindChimeModel().getThreadModels().get(i)
								.getBalls().size > 0) {
							selectedThread = world.getWindChimeModel()
									.getThreadModels().get(i);

							viewState = THREAD_VIEW;

							cam.zoom = THREAD_VIEW_ZOOM;

							cam.position.x = afterUnProject.x;
							cam.position.y = afterUnProject.y;

							for (ThreadModel threadModel : threadModels) {
								if (!(threadModel == selectedThread)) {
									threadModel.setVisible(false);
								}
							}
						} else {
							
							cam.position.x = afterUnProject.x;
							cam.position.y = afterUnProject.y;
							
							viewState = NEW_PLAYLIST_VIEW;
						}
					}
				}
				
/*				rectangle.x = world.getWindChimeModel().getThreadModels()
						.get(i).getSprite()
						.getBoundingRectangle().x;

				rectangle.y = world.getWindChimeModel().getThreadModels()
						.get(i).getSprite().getBoundingRectangle().y;

				rectangle.height = world.getWindChimeModel()
						.getThreadModels().get(i).getSprite()
						.getBoundingRectangle().height;

				rectangle.width = world.getWindChimeModel()
						.getThreadModels().get(0)
						.getSprite().getBoundingRectangle().getWidth();
				
				if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {
					itemTap = true;
					viewState = NEW_PLAYLIST_VIEW;
					
					cam.position.x = afterUnProject.x;
					cam.position.y = afterUnProject.y;
				}*/
								

			} else if (viewState == THREAD_VIEW) {

				for (int i = 0; i < selectedThread.getBalls().size; i++) {

					rectangle.x = selectedThread.getBalls().get(i).getSprite()
							.getBoundingRectangle().x;

					rectangle.y = selectedThread.getBalls().get(i).getSprite()
							.getBoundingRectangle().y;

					rectangle.height = selectedThread.getBalls().get(i)
							.getSprite().getBoundingRectangle().height;

					rectangle.width = selectedThread.getBalls().get(i)
							.getSprite().getBoundingRectangle().getWidth();

					if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {
						itemTap = true;
						viewState = BALL_VIEW;
						cam.zoom = BALL_VIEW_ZOOM;

						cam.position.x = rectangle.x + (rectangle.width / 2);
						cam.position.y = rectangle.y + (rectangle.height / 2);

						playSong(selectedThread.getBalls().get(i));

						particleEffect.getEmitters().first()
								.setPosition(cam.position.x, cam.position.y);
						particleEffect.start();

						selectedBall = selectedThread.getBalls().get(i);
					}

				}

			} else if (viewState == BALL_VIEW) {

				rectangle.x = selectedBall.getSprite().getBoundingRectangle().x;
				rectangle.y = selectedBall.getSprite().getBoundingRectangle().y;
				rectangle.height = selectedBall.getSprite()
						.getBoundingRectangle().height;
				rectangle.width = selectedBall.getSprite()
						.getBoundingRectangle().width;

				if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {
					itemTap = true;

					if (currentMusic.isPlaying())
						currentMusic.pause();
					else
						currentMusic.play();
				}
				
			} else if (viewState == NEW_PLAYLIST_VIEW) {
				
				afterUnProject.x = x;
				afterUnProject.y = y;

				playListCam.unproject(afterUnProject);
				
//				for (int i = 0; i < world.getNewPlayListThread().getBalls().size; i++) {
					
					rectangle.x = world.getNewPlayListThread().getBalls().get(0).getSprite()
							.getBoundingRectangle().x;

					rectangle.y = world.getNewPlayListThread().getSprite().getBoundingRectangle().y;

					rectangle.height = world.getNewPlayListThread().getSprite()
							.getBoundingRectangle().height;

					rectangle.width = world.getNewPlayListThread().getBalls().get(0)
							.getSprite().getBoundingRectangle().getWidth();
					
					if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {
						Gdx.app.log("Tap", "Add to new playlist");
						itemTap = true;			
					}
					
//				}
				
			}

			// Go Back
			if (!itemTap) {

				if (viewState == THREAD_VIEW) {

					for (ThreadModel threadModel : threadModels) {
						threadModel.setVisible(true);
					}

					viewState = CHIME_VIEW;

					cam.zoom = CHIME_VIEW_ZOOM;

					cam.position.set(cam.viewportWidth / 2f,
							(cam.viewportHeight / 2f), 0);
					cam.translate(0, -150, 0);

					// clear selected thread
					selectedThread = null;
				} else if (viewState == BALL_VIEW) {

					viewState = THREAD_VIEW;
					cam.zoom = THREAD_VIEW_ZOOM;

					cam.position.x = afterUnProject.x;
					cam.position.y = afterUnProject.y;
					
				} else if (viewState == NEW_PLAYLIST_VIEW) {
					
					viewState = CHIME_VIEW;

					cam.zoom = CHIME_VIEW_ZOOM;

					cam.position.set(cam.viewportWidth / 2f,
							(cam.viewportHeight / 2f), 0);
					
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * @param ballModel
	 */
	private void playSong(BallModel ballModel) {

		if (ballModel == selectedBall)
			return;

		if (currentMusic != null) {
			currentMusic.stop();
			currentMusic.dispose();
			currentMusic = null;
			
			currentDecoder.dispose();
			currentDecoder = null;
		}

		if (ballModel.getSongPath() != null) {
			currentMusic = Gdx.audio.newMusic(Gdx.files.external(ballModel
					.getSongPath()));
			currentMusic.play();
			
			 currentDecoder = new Mpg123Decoder(Gdx.files.external(ballModel
						.getSongPath()));
			 particleEffect.getEmitters().get(0).setMaxParticleCount((int) currentDecoder.getLength());
			 particleEffect.getEmitters().get(0).setMaxParticleCount(0);
			 
			 
		}
	}

	@Override
	public boolean longPress(float x, float y) {
		
		afterUnProject.x = x;
		afterUnProject.y = y;

		playListCam.unproject(afterUnProject);
		
		if (viewState == NEW_PLAYLIST_VIEW) {
			
			BallModel newBall = null;
			
			for (int i = 0; i < world.getNewPlayListThread().getBalls().size; i++) {
				
				rectangle.x = world.getNewPlayListThread().getBalls().get(i).getSprite()
						.getBoundingRectangle().x;
		
				rectangle.y = world.getNewPlayListThread().getBalls().get(i).getSprite()
						.getBoundingRectangle().y;
		
				rectangle.height = world.getNewPlayListThread().getBalls().get(i)
						.getSprite().getBoundingRectangle().height;
		
				rectangle.width = world.getNewPlayListThread().getBalls().get(i)
						.getSprite().getBoundingRectangle().getWidth();
				
				if (rectangle.contains(afterUnProject.x, afterUnProject.y)) {
					
					newBall = world.getNewPlayListThread().getBalls().get(i);
								
				}
				
			}
			
			if(newBall != null) {
				
				world.getNewPlayListThread().getBalls().removeValue(newBall, true);
				world.getWindChimeModel().getThreadModels().get(3).getBalls().add(newBall);
				
				float xPos = PlayerUtility.calculateThreadXStartPos()
						+ PlayerAssetManager.getThreadTexture().getWidth() / 2
						- (PlayerAssetManager.getBall().getWidth() / 2);
				
				xPos += (235 * 3);
				
				float ballsYPosStart = Gdx.graphics.getHeight()
						- PlayerAssetManager.getChimeHeadTexture().getHeight()
						- PlayerAssetManager.getThreadTexture().getHeight();
				ballsYPosStart -= 100;
				
				int floatdn = world.getWindChimeModel().getThreadModels().get(3).getBalls().size;
				
				ballsYPosStart += 200 * floatdn;
				
				newBall.getSprite().setPosition(xPos, ballsYPosStart);
			}

		}
		
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {

		if (viewState == THREAD_VIEW) {

			cam.translate(0, deltaY, 0);


			Gdx.app.log("Thread Bounds UnProject", "" + tempVector);
			
		} else if(viewState == NEW_PLAYLIST_VIEW) {
			playListCam.translate(0, deltaY, 0);
		}
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {

		tempVector.y = world.getWindChimeModel().getThreadModels().get(0)
				.getSprite().getY();
		cam.unproject(tempVector);
		Gdx.app.log("Thread Y", "" + tempVector.y);

		if (tempVector.y > Gdx.graphics.getHeight()) {

		}

		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		
		Gdx.app.log("Initial Distance", "" + initialDistance);
		Gdx.app.log("Distance", "" + "" + distance);
		Gdx.app.log("Difference", distance - initialDistance + "");
		
		if(currentMusic != null) {
			float newPos = currentMusic.getPosition() + (distance - initialDistance);
			if(newPos < 0) {
				newPos = 0;
			} else if(newPos > currentDecoder.getLength()) {
				newPos = currentDecoder.getLength();
			}

			if(currentMusic instanceof AndroidMusic) {
				((AndroidMusic)currentMusic).setPosition(newPos);
			}
		}
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void dispose() {

		if (currentMusic != null) {
			currentMusic.dispose();
		}

	}

	@Override
	public boolean keyDown(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
