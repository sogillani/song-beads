package com.ztc.player.songbeads.renderer;

/**
 * @author Omer Gillani
 *
 */
public abstract class PlayerRenderer {
	
	/**
	 * 
	 */
	abstract public void render();
	
	/**
	 * 
	 * @param width
	 * @param height
	 */
	abstract public void resize(int width, int height);
	
	/**
	 * 
	 */
	abstract public void dispose();

}
