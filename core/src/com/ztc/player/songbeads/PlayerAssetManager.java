package com.ztc.player.songbeads;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author Omer Gillani
 *
 */
public class PlayerAssetManager {
    
    private static Texture chimeHeadTexture;
    private static Texture threadTexture;
    private static Texture ball;
    private static Texture background;
    
	private static SpriteBatch spriteBatch;
    
	/**
	 * Load assets
	 */
	public static void load() {
		
		spriteBatch = new SpriteBatch();
		chimeHeadTexture = new Texture(Gdx.files.internal("data/chimehead.png"));
		threadTexture = new Texture(Gdx.files.internal("final/Thread.png"));
		ball = new Texture(Gdx.files.internal("final/ball1.png"));
		background = new Texture(Gdx.files.internal("final/back.jpg"));
		
	}
	
	/**
	 * 
	 * @return
	 */
	public static SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}
	
	/**
	 * 
	 * @return
	 */
	public static Texture getChimeHeadTexture() {
		return chimeHeadTexture;
	}
	
	/**
	 * 
	 * @return
	 */
	public static Texture getThreadTexture() {
		return threadTexture;
	}
	
	public static Texture getBall() {
		return ball;
	}
	
	public static Texture getBackground() {
		return background;
	}
	
	/**
	 * Dispose assets
	 */
	public static void dispose() {
		spriteBatch.dispose();
		chimeHeadTexture.dispose();
		threadTexture.dispose();
		ball.dispose();
		background.dispose();
	}
}
