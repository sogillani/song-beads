package com.ztc.player.songbeads;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

/**
 * 
 * @author Omer Gillani
 *
 */

public class SongBeadsPlayer extends Game {
	
//	SpriteBatch batch;
//	Texture img;
	
	@Override
	public void create () {
		
		Gdx.app.log("SongBeadsPlayer", "Created");
		
		PlayerAssetManager.load();
		
		setScreen(new PlayerScreen());
		/*
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		*/
	}

/*	@Override
	public void render () {
		
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
		
	}*/
	
	@Override
	public void dispose() {
		super.dispose();

		PlayerAssetManager.dispose();
	}
}
