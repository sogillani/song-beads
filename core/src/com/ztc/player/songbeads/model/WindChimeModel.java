package com.ztc.player.songbeads.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;

/**
 * @author Omer Gillani
 *
 */
public class WindChimeModel extends PlayerModel {
	
	/** Thread models **/
	Array<ThreadModel> threadModels = new Array<ThreadModel>();

	/**
	 * 
	 * @param sprite
	 */
	public WindChimeModel(Sprite sprite) {
		super(sprite);
	}
	
	/**
	 * 
	 * @return
	 */
	public Array<ThreadModel> getThreadModels() {
		return threadModels;
	}
	
	/**
	 * 
	 * @param threadModels
	 */
	public void setThreadModels(Array<ThreadModel> threadModels) {
		this.threadModels = threadModels;
	}
}
