package com.ztc.player.songbeads.model;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * @author Omer Gillani
 *
 */
public class BallModel extends PlayerModel {
	
	/** Song Path **/
	private String songPath;

	/**
	 * 
	 * @param sprite
	 */
	public BallModel(Sprite sprite) {
		super(sprite);
	}
	
	/**
	 * 
	 * @param songPath
	 */
	public void setSongPath(String songPath) {
		this.songPath = songPath;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSongPath() {
		return songPath;
	}
}
