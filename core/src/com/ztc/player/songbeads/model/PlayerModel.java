package com.ztc.player.songbeads.model;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * @author Omer Gillani
 *
 */
public class PlayerModel {
	
	/** Model sprite **/
	private Sprite sprite;
	
	/** Visible **/
	private boolean visible = true;

	/**
	 * 
	 * @param sprite
	 */
	public PlayerModel(Sprite sprite) {
		this.sprite = sprite;
	}
	
	/**
	 * 
	 * @return
	 */
	public Sprite getSprite() {
		return sprite;
	}
	
	/**
	 * 
	 * @param sprite
	 */
	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
