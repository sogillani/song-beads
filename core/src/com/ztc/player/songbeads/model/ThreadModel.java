package com.ztc.player.songbeads.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;


/**
 * @author Omer Gillani
 *
 */
public class ThreadModel extends PlayerModel {

	/** List of balls on thread **/
	private Array<BallModel> balls;
	
	/**
	 * 
	 * @param sprite
	 */
	public ThreadModel(Sprite sprite) {
		super(sprite);
		
		balls = new Array<BallModel>();
	}
	
	/**
	 * 
	 * @return
	 */
	public Array<BallModel> getBalls() {
		return balls;
	}
	
	/**
	 * 
	 * @param balls
	 */
	public void setBalls(Array<BallModel> balls) {
		this.balls = balls;
	}
	
}
