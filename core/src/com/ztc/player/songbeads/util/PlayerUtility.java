package com.ztc.player.songbeads.util;

import com.badlogic.gdx.Gdx;
import com.ztc.player.songbeads.PlayerAssetManager;

/**
 * @author Omer Gillani
 *
 */
public class PlayerUtility {
	
	/**
	 * 
	 * @return
	 */
	public static float calculateChimeHeadYPos() {
		return Gdx.graphics.getHeight()
				- PlayerAssetManager.getChimeHeadTexture().getHeight();
	}
	
	/**
	 * 
	 * @return
	 */
	public static float calculateChimeHeadXpos() {
		return Gdx.graphics.getWidth() / 2
				- (PlayerAssetManager.getChimeHeadTexture().getWidth() / 2);
	}
	
	public static float calculateThreadXStartPos() {
		float threadStartXPos = (Gdx.graphics.getWidth() / 2)
				- (PlayerAssetManager.getChimeHeadTexture().getWidth() / 2)
				+ 20;
		
		return threadStartXPos;
	}

}
